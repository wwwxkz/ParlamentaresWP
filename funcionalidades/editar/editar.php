<?php
require_once(DEPUTADOS_LOCAL . 'funcionalidades/editar/script.php');
require_once(DEPUTADOS_LOCAL . 'funcionalidades/editar/shortcode.php');
add_shortcode('deputados_editar', 'deputados_editar_shortcode');
add_action('admin_menu', 'deputados_editar_formulario');
add_action('template_redirect', 'deputados_editar_formulario');
?>