<?php
function deputados_editar_formulario()
{
    if (isset($_POST['editar']) || isset($_POST['deputados-editar'])) {
        global $wpdb;

        $id = $_POST['id'];

        $sql = "SELECT * FROM sna_deputados where id = $id";
        $usuario = $wpdb->get_results($sql);

        $wpdb->replace(
            'sna_deputados',
            array(
                'id' => $_POST['id'],
                'nome' => $usuario[0]->nome,
                'partido' => $usuario[0]->partido,
                'uf' => $usuario[0]->uf,
                'titular' => $usuario[0]->titular,
                'endereco_1' => $usuario[0]->endereco_1,
                'anexo' => $usuario[0]->anexo,
                'endereco_2' => $usuario[0]->endereco_2,
                'gabinete' => $usuario[0]->gabinete,
                'endereco_3' => $usuario[0]->endereco_3,
                'telefone' => $usuario[0]->telefone,
                'fax' => $usuario[0]->fax,
                'mes' => $usuario[0]->mes,
                'dia' => $usuario[0]->dia,
                'email' => $usuario[0]->email,
                'nome_sem_acento' => $usuario[0]->nome_sem_acento,
                'tratamento' => $usuario[0]->tratamento,
                'nome_civil' => $usuario[0]->nome_civil,
                'acao_1' => $_POST['deputados-acao-1'],
				'url' => $_POST['url'],
            )
        );

        return 0;
    }
}