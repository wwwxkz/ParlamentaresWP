<?php

// Este plugin suporta senadores e deputados, há um "like email dep" em praticamente todas as buscas
// para ignorar os senadores já que foi decidido acompanhar ambos separadamente 

// Verifica se agente é IOS ou IpadOS para que o CSS se ajuste ao renderizador do navegador

$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$iPod_s    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod Simulator");
$iPhone_s  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone Simulator");
$iPad_s    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad Simulator");

// Verifica se usuario IOS / Android está utilizando webview, caso esteja, o ofereça para abrir no navegador

$isWebView = false;
if((strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile/') !== false) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari/') == false)) :
    $isWebView = true;
elseif(isset($_SERVER['HTTP_X_REQUESTED_WITH'])) :
    $isWebView = true;
endif;

if($isWebView)  {
	$file = 'https://campanha.aeronautas.org.br/';
	header('Content-type: text/html');
	header('Content-Disposition: attachment; filename="Justificativa_Aposentadoria_Especial_Aeronautas.pdf"');
	@readfile($file);
}

// Problemas com alguma funcionalidade ? acesse a pagina de fallback com os dados em RAW

$fallback = $_GET['fallback'];

if ($fallback == '1') {
	$lista = $wpdb->get_results("SELECT `email` FROM `sna_deputados` WHERE `acao_1` = 0 AND email like 'dep%' LIMIT 499;");
	$emails = '';
	echo '<head><meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>';
	echo '
	    <h2>
        Continuidade da campanha pela aposentadoria especial dos Aeronautas
    	</h2>
	';
	echo '<a style="color: red">Obs: alguns aplicativos de e-mail tem limite do número de destinatários de uma única mensagem. Se seu e-mail com a solicitação da indicação não for entregue, envie o e-mail apenas para metade dos deputados e depois para o restante !</a>';
	echo '<h2>Remetentes (E-mail dos deputados)<h2>';
	echo '<h4 style="margin-bottom: 5px;">Selecione tudo e copie</h4>';
	foreach ($lista as $deputado) {
		$emails .= $deputado->email . ';';
	}
	echo '<textarea style="width: 100%; min-height: 400px;">';
	echo $emails;
	echo '</textarea>';
	echo '<h2>Corpo do e-mail<h2>';
	echo '<h4 style="margin-bottom: 5px;">Selecione tudo e copie</h4>';
	echo '<textarea style="width: 100%; min-height: 400px;">';
	echo 'Ao Exmo. Sr(a) Parlamentar, Venho por meio deste solicitar a V.Exa. a apresentação de uma Indicação ao Exmo. Sr. Presidente da República, Jair Messias Bolsonaro, para que altere, por meio de decreto presidencial, o item 2.0.5 do Anexo IV do Regulamento da Previdência Social, com o objetivo de incluir o trabalho embarcado em aeronave com cabine pressurizada na classificação de atividade sujeita a agentes nocivos, referente à pressão atmosférica anormal, possibilitando, assim, que os tripulantes possam conseguir a aposentadoria especial na via administrativa. Destaco que o SNA (Sindicato Nacional dos Aeronautas), tem por função legal e institucional a promoção de ações que visem a manutenção e a melhoria das condições laborais e sociais dos aeronautas, protocolou em 14 de julho de 2022, uma petição com mais de 10 mil assinaturas visando essa alteração no regulamento previdenciário. Atualmente os aeronautas têm seu processo de aposentadoria negado de forma administrativa pelo INSS ao completar 25 anos de atividade, porém judicialmente têm obtido numerosos êxitos, a ponto de o STJ determinar um entendimento majoritário em favor dos aeronautas sobre o tema. Em função de se ver vencido judicialmente, o governo acaba sofrendo um grande prejuízo pois tem de indenizar os aeronautas com juros judiciais e honorários advocatícios. Para informações mais detalhadas e visando oferecer ao nobre parlamentar toda a Justificativa de nosso pedido, envio, em anexo, um documento cuidadosamente preparado pelo Sindicato Nacional dos Aeronautas acerca do tema em questão, além de uma sugestão de Indicação. Pelo exposto, peço a V.Exa. que atenda a referida solicitação. Atenciosamente, (COLOQUE SEU NOME AQUI)';
		echo '</textarea>';
	echo '<h2>Documentos<h2>';
	echo '<h4 style="margin-bottom: 5px;">Copie o endereço e cole no navegador</h4>';
	echo '<h5>';
	echo '<a href="https://campanha.aeronautas.org.br/wp-content/uploads/2022/07/Justificativa_Aposentadoria_Especial_Aeronautas.pdf">Justificativa Aposentadoria Especial Aeronautas</a>';
	echo '<br><a>https://campanha.aeronautas.org.br/wp-content/uploads/2022/07/Justificativa_Aposentadoria_Especial_Aeronautas.pdf</a>';
	echo '<br><br><a href="https://campanha.aeronautas.org.br/wp-content/uploads/2022/07/Indicacao_Aposentadoria_Especial_Aeronautas.docx">Indicacao Aposentadoria Especial Aeronautas</a>';
	echo '<br><a>https://campanha.aeronautas.org.br/wp-content/uploads/2022/07/Indicacao_Aposentadoria_Especial_Aeronautas.docx</a>';
	echo '</h5>';
	return;
}

if ($fallback == '2') {
	echo '<script>
	let texto = `Ao Exmo. Sr(a) Parlamentar,\n\nVenho por meio deste solicitar a V.Exa. a apresentação de uma Indicação ao Exmo. Sr. Presidente da República, Jair Messias Bolsonaro, para que altere, por meio de decreto presidencial, o item 2.0.5 do Anexo IV do Regulamento da Previdência Social, com o objetivo de incluir o trabalho embarcado em aeronave com cabine pressurizada na classificação de atividade sujeita a agentes nocivos, referente à pressão atmosférica anormal, possibilitando, assim, que os tripulantes possam conseguir a aposentadoria especial na via administrativa.\n\nDestaco que o SNA (Sindicato Nacional dos Aeronautas), tem por função legal e institucional a promoção de ações que visem a manutenção e a melhoria das condições laborais e sociais dos aeronautas, protocolou em 14 de julho de 2022, uma petição com mais de 10 mil assinaturas visando essa alteração no regulamento previdenciário. \n\nAtualmente os aeronautas têm seu processo de aposentadoria negado de forma administrativa pelo INSS ao completar 25 anos de atividade, porém judicialmente têm obtido numerosos êxitos, a ponto de o STJ determinar um entendimento majoritário em favor dos aeronautas sobre o tema. Em função de se ver vencido judicialmente, o governo acaba sofrendo um grande prejuízo pois tem de indenizar os aeronautas com juros judiciais e honorários advocatícios.\n\nPara informações mais detalhadas e visando oferecer ao nobre parlamentar toda a Justificativa de nosso pedido, envio, em anexo, um documento cuidadosamente preparado pelo Sindicato Nacional dos Aeronautas acerca do tema em questão, além de uma sugestão de Indicação.\n\nPelo exposto, peço a V.Exa. que atenda a referida solicitação.\n\nAtenciosamente,`;
    let nome = prompt("Qual seu nome?");
	
    var divStr = "<div class=\'text-warning\'>" + texto + "</div>";
	document.getElementsByTagName(\'body\')[0].innerHTML += divStr;
	
	</script>';
	return;
}

// Disponibiliza donwload para usuarios de androids e navegadores com bloqueio de href

$arquivo = $_GET['download'];

if ($arquivo == '1') {
	$file = 'wp-content/uploads/2022/07/Justificativa_Aposentadoria_Especial_Aeronautas.pdf';
	if (file_exists($file)) {
		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename="Justificativa_Aposentadoria_Especial_Aeronautas.pdf"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($file));
		header('Accept-Ranges: bytes');
		@readfile($file);
	}
}

if ($arquivo == '2') {
	$file = 'wp-content/uploads/2022/07/Indicacao_Aposentadoria_Especial_Aeronautas.docx';
	if (file_exists($file)) {
		header('Content-type: application/docx');
		header('Content-Disposition: inline; filename="Indicacao_Aposentadoria_Especial_Aeronautas.docx"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($file));
		header('Accept-Ranges: bytes');
		@readfile($file);
	}
}

// Shortcode

function deputados_editar_shortcode()
{
    global $wpdb;
	global $wp_query;
    // Pega todos os deputados e a contagem dos que indicarão
    $deputados = $wpdb->get_results("SELECT COUNT(*) FROM `sna_deputados` where email like 'dep%';");
    $deputados_fez = $wpdb->get_results("SELECT COUNT(*) FROM `sna_deputados` where `acao_1` = 1 AND email like 'dep%';");
    // Area de Transferencia Mobile e Internet Explorer  
    echo '
    <script>
    // Mobile ajuste
    function copiarTransferenciaMobile(text) {
        const elem = document.createElement("textarea");
        elem.value = text;
        document.body.appendChild(elem);
        elem.select();
        document.execCommand("copy");
        document.body.removeChild(elem);
     }
	 // Iphone IOS and Safari 
	function copyToClipboard(string) {
	  let textarea;
	  let result;

	  try {
		textarea = document.createElement("textarea");
		textarea.setAttribute("readonly", true);
		textarea.setAttribute("contenteditable", true);
		textarea.style.position = "fixed"; // prevent scroll from jumping to the bottom when focus is set.
		textarea.value = string;

		document.body.appendChild(textarea);

		textarea.focus();
		textarea.select();

		const range = document.createRange();
		range.selectNodeContents(textarea);

		const sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);

		textarea.setSelectionRange(0, textarea.value.length);
		result = document.execCommand("copy");
	  } catch (err) {
		console.error(err);
		result = null;
	  } finally {
		document.body.removeChild(textarea);
	  }

	  return true;
	}
	
	function baixarArquivo(url) { window.location.href = url; };
	
     // Edge e IE ajuste
     function copiarTransferenciaIE(text) {
        var controlValue = text;
            var textare = document.createElement("textarea");
            textare.textContent = controlValue;
            textare.style.position = "fixed";
            document.body.appendChild(textare);
            textare.focus();
            textare.select();
            try {
                window.focus();
                navigator.clipboard.writeText(textare.textContent);
            }
            catch (error) {
                console.error(error);
            }
            finally {
                document.body.removeChild(textare);
            }
     }
	 // Last IOS TRY
	 function iosClipboard(text) {
	 	navigator.clipboard.writeText(text);
	 }
    </script>
	<meta http-equiv="Content-Disposition" content="attachment; filename=filename.extesion"/>';
	
    // Modal para obter nome do aeronauta
    
    echo '
    <div id="myModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <div id="modal-delete" style="display: flex;margin-bottom: 20px; margin-top: 20px; flex-direction: row; border-radius: 10px;">
                <input id="deputado-pesquisa-2" style="border: 2px solid #c9c9c9 !important; width: 80%; padding: 15px;  border-radius: 10px; border-top-right-radius: 0; border-bottom-right-radius: 0; flex-grow 1;" placeholder="Seu nome para assinatura"></input>
                <i onclick="copiarTransferencia()" class="fa-solid fa-magnifying-glass"></i>
                <button id="deputado-pesquisa-botao" onclick="copiarTransferencia()">Copiar</button>
            </div>
        </div>
    </div>
    ';
	
	// Quer ver os parlamentares que já indicaram ?? 
	
    if (isset($_GET['indicaram'])) {
		echo '<script>
			const element = document.getElementById("myModal");
			element.remove();
		</script>';
// 		echo '<div style="border: 1px solid #c9c9c9; border-radius: 10px; padding: 20px; padding-top: 0;">';
// 		echo '<h2>Senadores que já indicaram: </h2>';
// 		echo '<div style="display: flex; flex-direction: row; flex-wrap: wrap;">';
// 		$lista = $wpdb->get_results("SELECT `nome` FROM `sna_deputados` WHERE `acao_1` = 1 AND email like 'sen%';");
//         foreach ($lista as $deputado) {
// 			echo '<div id="ja-feitos">';
//             echo $deputado->nome;
// 			echo '</div>';
//         }
// 		echo '</div>';
// 		echo '</div>';
		echo '<div style="margin-top: 20px; border: 1px solid #c9c9c9; border-radius: 10px; padding: 20px; padding-top: 0;">';
		echo '<h2>Deputados que já indicaram: </h2>';
		echo '<div style="display: flex; flex-direction: row; flex-wrap: wrap;">';
		$lista = $wpdb->get_results("SELECT `nome`, `url` FROM `sna_deputados` WHERE `acao_1` = 1 AND email like 'dep%';");
        foreach ($lista as $deputado) {
			echo '<div id="ja-feitos">';
			echo '<a href="' . $deputado->url . '" target="_blank">';
            echo $deputado->nome;
			echo '</a>';
			echo '</div>';
        }
		echo '</div>';
		echo '</div>';
	     echo '<div style="margin-top: 20px; width: max-content;margin-bottom: 10px;padding: 10px; background-color: #be1e21; border-radius: 10px; margin-right: 5px;">';
            echo '<a style="color: white; text-decoration: none;" href="https://campanha.aeronautas.org.br/parlamentares/">Voltar</a>';
        echo '</div>';
		
		echo '
		<style>
			#ja-feitos {
				margin-top: 10px; 
				flex: 0 0 25%;
			}
			 @media (max-width: 780px)
			{
				#ja-feitos {
					margin-top: 10px; 
					flex: 0 0 100%;
				}
			}
		</style>
		';
		return;
    }
    // Quer lista? a copie para area de transferencia 
    if (isset($_GET['lista'])) {
        $lista = $wpdb->get_results("SELECT `email` FROM `sna_deputados` WHERE `acao_1` = 0 AND `email` like 'dep%';");
        $emails = '';
        foreach ($lista as $deputado) {
            $emails .= $deputado->email . ';';
        }
        echo '
        <script>
            navigator.clipboard.writeText(`' . $emails . '`);
            // Chama clips alternativos
            copiarTransferenciaMobile(`' . $emails . '`);
            copiarTransferenciaIE(`' . $emails . '`);
            var modal = document.getElementById("myModal");
            modal.style.display = "block";
            document.getElementById("modal-delete").innerHTML = "E-mails dos parlamentares que ainda não indicaram a petição copiados com sucesso, basta colar estes emails na caixa de endereços do seu e-mail.";
        </script>
        ';
    }
    // Pega argumentos de pesquisa e insere na query 
    $filter = '';
	
	if(isset($_GET['partido'])) {
		if ($filter != '') {
			$filter = ' AND partido = \'' . $_GET['partido'] . '\''. " AND email like 'dep%'";
		} else {
			$filter = ' WHERE partido = \'' . $_GET['partido'] . '\''. " AND email like 'dep%'";
		}
	} 
    if (isset($_GET['nome'])) {
		if ($filter != '') {
			$filter .= ' AND nome like \'%' . $_GET['nome'] . '%\' AND email like \'dep%\'';
		} else {
			$filter = ' WHERE nome like \'%' . $_GET['nome'] . '%\' AND email like \'dep%\'';
		}
    }
    if (isset($_GET['parlamentar'])) {
		if ($filter != '') {
			if ($_GET['parlamentar'] == 'deputados') {
				$filter .= " AND email like 'dep%'";
			}
// 			if ($_GET['parlamentar'] == 'senadores') {
// 				$filter .= " AND email like 'sen%'";
// 			}
		} else {
			if ($_GET['parlamentar'] == 'deputados') {
				$filter = " WHERE email like 'dep%'";
			}
// 			if ($_GET['parlamentar'] == 'senadores') {
// 				$filter = " WHERE email like 'sen%'";
// 			}
		}
    }
    $sql = "SELECT * FROM sna_deputados" . $filter;
    $total_query = "SELECT COUNT(1) FROM (${sql}) AS combined_table";
    $total = $wpdb->get_var($total_query);
    $items_per_page = 24;
    $page = isset($_GET['cpage']) ? abs((int) $_GET['cpage']) : 1;
    $offset = ($page * $items_per_page) - $items_per_page;
    $usuarios = $wpdb->get_results($sql . " ORDER BY id LIMIT ${offset}, ${items_per_page}");
    // Abre container e adiciona fontawesome para os icones
    echo '
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <div id="deputados">
    <div id="deputados-container">
    ';
    // Função para copiar texto para area de transferencia 
    $lista = $wpdb->get_results("SELECT `email` FROM `sna_deputados` WHERE `acao_1` = 0;");
        $emails = '';
        foreach ($lista as $deputado) {
            $emails .= $deputado->email . ';';
        };
	
    echo '
        <script>
          function copiarTransferenciaLista() {
                //window.location.href = "https://projetos.aeronautas.org.br/parlamentares/?lista=true";				
				navigator.clipboard.writeText(`' . $emails . '`);
            // Chama clips alternativos
            copiarTransferenciaMobile(`' . $emails . '`);
            copiarTransferenciaIE(`' . $emails . '`);
            var modal = document.getElementById("myModal");
            modal.style.display = "block";
            document.getElementById("modal-delete").innerHTML = "E-mails dos parlamentares que ainda não indicaram a petição copiados com sucesso, basta colar estes emails na caixa de endereços do seu e-mail.";
            }
            function copiarTransferencia() {
                let texto = `Ao Exmo. Sr(a) Parlamentar,\n\nVenho por meio deste solicitar a V.Exa. a apresentação de uma Indicação ao Exmo. Sr. Presidente da República, Jair Messias Bolsonaro, para que altere, por meio de decreto presidencial, o item 2.0.5 do Anexo IV do Regulamento da Previdência Social, com o objetivo de incluir o trabalho embarcado em aeronave com cabine pressurizada na classificação de atividade sujeita a agentes nocivos, referente à pressão atmosférica anormal, possibilitando, assim, que os tripulantes possam conseguir a aposentadoria especial na via administrativa.\n\nDestaco que o SNA (Sindicato Nacional dos Aeronautas), tem por função legal e institucional a promoção de ações que visem a manutenção e a melhoria das condições laborais e sociais dos aeronautas, protocolou em 14 de julho de 2022, uma petição com mais de 10 mil assinaturas visando essa alteração no regulamento previdenciário. \n\nAtualmente os aeronautas têm seu processo de aposentadoria negado de forma administrativa pelo INSS ao completar 25 anos de atividade, porém judicialmente têm obtido numerosos êxitos, a ponto de o STJ determinar um entendimento majoritário em favor dos aeronautas sobre o tema. Em função de se ver vencido judicialmente, o governo acaba sofrendo um grande prejuízo pois tem de indenizar os aeronautas com juros judiciais e honorários advocatícios.\n\nPara informações mais detalhadas e visando oferecer ao nobre parlamentar toda a Justificativa de nosso pedido, envio, em anexo, um documento cuidadosamente preparado pelo Sindicato Nacional dos Aeronautas acerca do tema em questão, além de uma sugestão de Indicação.\n\nPelo exposto, peço a V.Exa. que atenda a referida solicitação.\n\nAtenciosamente,`;
                let nome = document.getElementById("deputado-pesquisa-2").value;
                let clipboard = texto + "\n" + nome;
                // Clip principal
                navigator.clipboard.writeText(clipboard);
                // Chama clips alternativos
                copiarTransferenciaMobile(clipboard);
                copiarTransferenciaIE(clipboard);
				var iOS = ["iPad", "iPhone", "iPod", "iPad Simulator", "iPhone Simulator", "iPod Simulator"].indexOf(navigator.platform) >= 0;
				if (iOS) {
					copyToClipboard(clipboard);
					iosClipboard(clipboard);
				}
					document.getElementById("modal-delete").innerHTML = "O conteúdo assinado em nome de: \"" + nome + "\" foi copiado com sucesso, basta colar este texto no corpo do e-mail.";
				}
        </script>
    ';

    // Bloco de instruções
    echo '<div id="instrucoes">';
    echo '<div id="instrucoes-esquerda">';
			// Titulo
	echo '<div id="instrucoes-esquerda-titulo">';
    echo '
    <h2>
        Continuidade da campanha pela aposentadoria especial dos Aeronautas
    </h2>
    ';
    // Descrição
    echo '
    <p>
        Caros colegas, dando seguimento aos nossos esforços pela obtenção do direito dos Aeronautas de ter a aposentadoria especial aceita diretamente pela via administrativa, <b>solicitamos que todos os Aeronautas sigam o passo a passo descrito no box de procedimento</b> e solicitem a <a href="https://www.congressonacional.leg.br/legislacao-e-publicacoes/glossario-legislativo/-/legislativo/termo/indicacao">Indicação</a> por parte dos Parlamentares ao Exmo. Sr. Presidente da República em prol de nossa demanda.
    </p>
    ';
	echo '</div>';
	// Instruções iniciais
	echo '<div id="instrucoes-esquerda-titulo" style="margin-top: 20px";>';

    echo '
	<h4>
    	Corpo do E-mail
    </h4> 
    <a>
        Ao Exmo. Sr(a) Parlamentar,
        <br><br>   
        Venho por meio deste solicitar a V.Exa. a apresentação de uma Indicação ao Exmo. Sr. Presidente da República, Jair Messias Bolsonaro, para que altere, por meio de decreto presidencial, o item 2.0.5 do Anexo IV do Regulamento da Previdência Social, com o objetivo de incluir o trabalho embarcado em aeronave com cabine pressurizada na classificação de atividade sujeita a agentes nocivos, referente à pressão atmosférica anormal, possibilitando, assim, que os tripulantes possam conseguir a aposentadoria especial na via administrativa.
        <br><br>
        Destaco que o SNA (Sindicato Nacional dos Aeronautas), tem por função legal e institucional a promoção de ações que visem a manutenção e a melhoria das condições laborais e sociais dos aeronautas, protocolou em 14 de julho de 2022, uma petição com mais de 10 mil assinaturas visando essa alteração no regulamento previdenciário. 
        <br><br>
        Atualmente os aeronautas têm seu processo de aposentadoria negado de forma administrativa pelo INSS ao completar 25 anos de atividade, porém judicialmente têm obtido numerosos êxitos, a ponto de o STJ determinar um entendimento majoritário em favor dos aeronautas sobre o tema. Em função de se ver vencido judicialmente, o governo acaba sofrendo um grande prejuízo pois tem de indenizar os aeronautas com juros judiciais e honorários advocatícios.
        <br><br>
        Para informações mais detalhadas e visando oferecer ao nobre parlamentar toda a justificativa de nosso pedido, envio, em anexo, um documento cuidadosamente preparado pelo Sindicato Nacional dos Aeronautas acerca do tema em questão. 
		<br><br>
		Pelo exposto, peço a V.Exa. que atenda a referida solicitação.
        <br><br>
        Atenciosamente,
        <br>
        ______________________________
   </a>
   ';
    echo '</div>';
    echo '</div>';
	// Instrucoes direita
    echo '<div id="instrucoes-direita">';
		if( $iPod == true || $iPhone == true || $iPad == true || $iPod_s == true || $iPhone_s == true || $iPad_s == true ){
					echo '
   <!--  <div id="procedimento">
        <h4>
            Vídeo Tutorial
        </h4>
		<a href="..\wp-content\plugins\sna-deputados\funcionalidades\editar\assets\images\Tutorial.mp4">Veja o vídeo tutorial</a>
    </div> -->
    ';
		} else {
				echo '
  <!--  <div id="procedimento">
        <h4>
            Vídeo Tutorial
        </h4>
		<video width="440px" height="210px" controls>
		  <source src="..\wp-content\plugins\sna-deputados\funcionalidades\editar\assets\images\Tutorial.mp4">
		Seu navegador não suporta a execução de vídeos.
		</video>
		
    </div>-->
    ';
}  

    echo '
    <div id="procedimento">
        <h4>
            Procedimento Passo a Passo
        </h4> 
        <a>
            1 - Copiar todos os emails dos parlamentares sem indicação, clicando no botão "E-mails";
            <br><br>
            2 - Copiar o conteúdo do e-mail, clicando no botão "Corpo do E-mail";
            <br><br>
            3 - Incluir em anexo o arquivo em PDF com a justificativa.
			<br><br>
            4 - Incluir em anexo o documento com a sugestão de indicação.
			';
		// Fix iphone bugs
	echo '<script>
			function paginaFallback(n) {
				if(n == 1) {
					window.location.href = "https://campanha.aeronautas.org.br/parlamentares/?fallback=1";
				}
				if(n == 2) {
					window.location.href = "https://campanha.aeronautas.org.br/parlamentares/?fallback=2";
				}
				if(n == 3) {
					window.location.href = "https://campanha.aeronautas.org.br/parlamentares/?fallback=3";
				}
			}
		</script>';
	echo '<br><br><a style="color: red;">Obs: na versão mobile, devido aos diversos tipos de modelos, sistemas operacionais e versões, é possível que a forma automatizada de cópia dos e-mails não funcione.  <a style="color: #1a447a; text-decoration: underline;" onclick="paginaFallback(1)"> Clique aqui</a> <a style="color: red;">se isso ocorrer</a></a>';
	echo '
        </a>
    </div>
    ';

    echo '
   <div id="baixe">
        
        <h4>Clique abaixo para copiar</h4><div style="display: flex;flex-direction: row;justify-content: center;justify-content: space-evenly;/* min-width: 167px; */column-gap: 10px;">
                <div style="display: flex;justify-content: center;align-items: center;height: 24px;width: 50%;">
                    <center style="width: 100%;">
                        <button onclick="copiarTransferenciaLista()" style="cursor: pointer;display: flex;justify-content: center;align-items: center;width: 100%;height: 26px;border-radius: 5px;background-color: #1a447a;color: #ffff;border: none;box-shadow: 0 0 1em #a6a6a6;">
                            <i class="fa-solid fa-download"></i>&nbsp;
                            E-mails
                        </button>	
                    </center>
                </div>
                <div style="display: flex;justify-content: center;align-items: center;height: 50%;height: 24px;width: 50%;">
                    <center style="width: 100%">
                        <button id="myBtn" style="cursor: pointer;display: flex;justify-content: center;align-items: center;width: 100%;height: 26px;border-radius: 5px;background-color: #1a447a;color: #ffff;border: none;box-shadow: 0 0 1em #a6a6a6;">
                            <i class="fa-solid fa-download"></i>&nbsp;
                            Corpo do E-mail
                        </button>	
                    </center>
                </div>
            </div><h4>
            Baixe os documentos
        </h4><div id="baixe-botoes">
            <div>
                <center>
				             <img class="thumbnail" src="https://campanha.aeronautas.org.br/wp-content/uploads/2022/07/Justificativa_Aposentadoria_Especial_Aeronautas-pdf.jpg" style="max-width:100%; padding-bottom: 5px; cursor: pointer;" alt="" onclick="baixarArquivo(\'wp-content/uploads/2022/07/Justificativa_Aposentadoria_Especial_Aeronautas.pdf\')">
                    <a href="?download=1" download="" rel="noopener noreferrer" target="_blank">
                        Justificativa
                    </a>					
                </center>
            </div>
			
			<div>
                <center>
				             <img class="thumbnail" src="http://campanha.aeronautas.org.br/wp-content/uploads/2022/07/capaIndicacao.png" style="max-width:100%; padding-bottom: 5px; cursor: pointer;" alt="" onclick="baixarArquivo(\'wp-content/uploads/2022/07/Indicacao_Aposentadoria_Especial_Aeronautas.docx\')">
                    <a href="?download=2"  download="" rel="noopener noreferrer" target="_blank">
                        Indicação
                    </a>					
                </center>
            </div>
			
			
            
        </div>
    </div>
    ';
    echo '</div>';
    echo '</div>';
    // Configuração do modal de nome para ser inserido em texto copiado para area de transferencia
    echo '
    <script>
        function resetModal() { 
            document.getElementById("modal-delete").innerHTML = `
                <input id="deputado-pesquisa-2" style="border: 2px solid #c9c9c9 !important; width: 80%; padding: 15px;  border-radius: 10px; border-top-right-radius: 0; border-bottom-right-radius: 0; flex-grow 1;" placeholder="Seu nome para assinatura"></input>
                <i onclick="copiarTransferencia()" class="fa-solid fa-magnifying-glass"></i>
                <button id="deputado-pesquisa-botao" onclick="copiarTransferencia()">Copiar</button>
            `;
        }
        var modal = document.getElementById("myModal");
        var btn = document.getElementById("myBtn");
        var span = document.getElementsByClassName("close")[0];
        btn.onclick = function() {
            modal.style.display = "block";
        }
        span.onclick = function() {
            modal.style.display = "none";
            //resetModal();
			window.location.href = "https://campanha.aeronautas.org.br/";
        }
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
                //resetModal();
				window.location.href = "https://campanha.aeronautas.org.br/";
            }
        }
    </script>
    ';
    // Barra de progresso
    echo '
	<div style="display: flex; flex-direction: row; width: 100%; text-align: center; justify-content: center; align-items: center;">
		<h4><b>' . number_format(reset($deputados_fez[0]), 0, '', '.') . '</b> indicações realizadas. Total de parlamentares: <b>' . number_format(reset($deputados[0]), 0, '', '.') . '</b></h4>
		<a style="margin-left: 10px;" href="https://campanha.aeronautas.org.br/parlamentares/?indicaram=true">Clique aqui para ver os parlamentares que já indicaram</a>
	</div>
    <div style="width: 100%; height: 30px; border: 1px solid #c9c9c9; background-color: #e5e5e5; border-radius: 10px;">
        <div style="display: flex; border-radius: 10px; background-color: #1e73be; width: ' . round(reset($deputados_fez[0]) / number_format(reset($deputados[0]), 0, '', '.') * 100) . '%; height: 100%; align-items: center; justify-content: center;">
        <h5 style="margin: 0; color: white;">' . round(reset($deputados_fez[0]) / number_format(reset($deputados[0]), 0, '', '.') * 100) . '%</h5>
        </div>
    </div>
    ';
	// Texto de ajuda antes do filtro
	echo '<h2 style="width: 100%; text-align: center;"><b>Consulte os parlamentares aqui</b></h2>';
    // Filtro por Partido 
    // QUICK FIX IN include nome
    echo '<script>
    function redirect() {
		let url = "' . home_url($_SERVER['REQUEST_URI']) . '"; 
		if(url.includes("?")) {
			if(url.includes("nome")) {
				let pattern_primeiro_argumento = /\?nome=(.*?)\W/;
				let nome_primeiro_argumento = url.match(pattern_primeiro_argumento);
				let pattern_unico_argumento = /\?nome=(.*?)$/;
				let nome_unico_argumento = url.match(pattern_unico_argumento);
				let pattern_dentre_argumentos = /&nome=(.*?)\W/;
				let nome_dentre_argumentos = url.match(pattern_dentre_argumentos);
				let pattern_ultimo_argumento = /&nome=(.*?)$/;
				let nome_ultimo_argumento = url.match(pattern_ultimo_argumento);
				if (nome_primeiro_argumento !== null) {
					if (nome_primeiro_argumento.length > 0) {
						url = url.replace(nome_primeiro_argumento[1], document.getElementById("deputado-pesquisa").value);
						window.location.href = url;
					}
				}
				else if (nome_unico_argumento !== null) {
					if (nome_unico_argumento.length > 0) {
						url = url.replace(nome_unico_argumento[1], document.getElementById("deputado-pesquisa").value);
						window.location.href = url;
					}
				}
				else if (nome_dentre_argumentos !== null) {
					if (nome_dentre_argumentos.length > 0) {
						url = url.replace(nome_dentre_argumentos[1], document.getElementById("deputado-pesquisa").value);
						window.location.href = url;
					}
				}
				else if (nome_ultimo_argumento !== null) {
					if (nome_ultimo_argumento.length > 0) {
						url = url.replace(nome_ultimo_argumento[1], document.getElementById("deputado-pesquisa").value);
						window.location.href = url;
					}
				}
				else {
					window.location.href = "https://campanha.aeronautas.org.br/parlamentares/" + "?nome=" + document.getElementById("deputado-pesquisa").value;
				}
			} else {
				window.location.href = url + "&nome=" + document.getElementById("deputado-pesquisa").value;
			}
		} else {
			window.location.href = url + "?nome=" + document.getElementById("deputado-pesquisa").value;
		}
    }
    </script>';
    $partidos = $wpdb->get_results("SELECT DISTINCT `partido` FROM `sna_deputados` ORDER BY `partido` ASC;");
    echo '<div style="max-width: 915px; align-self: center; display: flex; flex-direction: column; margin-top: 20px; ">';
    echo '<div style="display: flex; flex-direction: row; flex-wrap: wrap; justify-content: center;">';
	$url = home_url($_SERVER['REQUEST_URI']);
    foreach ($partidos as $partido) {
		if(preg_match("/$partido->partido\W/", $url) || preg_match("/$partido->partido$/", $url)) {
			echo '<div style="margin-bottom: 10px;padding: 10px; background-color: #1a447a; border-radius: 10px; margin-right: 5px;">';
		} else {
			echo '<div style="margin-bottom: 10px;padding: 10px; background-color: #b3b2b2; border-radius: 10px; margin-right: 5px;">';
		}
        echo '<a style="color: white; text-decoration: none;" href="' . esc_url(add_query_arg('partido', $partido->partido,  home_url($_SERVER['REQUEST_URI']))) . '">' . $partido->partido . '</a>';
        echo '</div>';
    }
    echo '</div>';
	// Filtro Senadores - Deputados 
    echo '<div style="display: flex; flex-direction: row; flex-wrap: wrap; justify-content: center; margin-top: 20px; ">';
	/*	if(str_contains($url, 'deputados')) {
			echo '<div style="margin-bottom: 10px;padding: 10px; background-color: #1e73be; border-radius: 10px; margin-right: 5px;">';
		} else {
			echo '<div style="margin-bottom: 10px;padding: 10px; background-color: #b3b2b2; border-radius: 10px; margin-right: 5px;">';
		}
			echo '<a style="color: white; text-decoration: none;" href="' . esc_url(add_query_arg('parlamentar', 'deputados',  home_url($_SERVER['REQUEST_URI']))) . '">Deputados</a>';
        echo '</div>';
		if(str_contains($url, 'senadores')) {
       		echo '<div style="margin-bottom: 10px;padding: 10px; background-color: #1e73be; border-radius: 10px; margin-right: 5px;">';
		} else {
       		echo '<div style="margin-bottom: 10px;padding: 10px; background-color: #b3b2b2; border-radius: 10px; margin-right: 5px;">';
		}
            echo '<a style="color: white; text-decoration: none;" href="' . esc_url(add_query_arg('parlamentar', 'senadores',  home_url($_SERVER['REQUEST_URI']))) . '">Senadores</a>';
        echo '</div>';
	*/
        echo '<div style="margin-bottom: 10px;padding: 10px; background-color: #be1e21; border-radius: 10px; margin-right: 5px;">';
            echo '<a style="color: white; text-decoration: none;" href="https://campanha.aeronautas.org.br/parlamentares/">Limpar</a>';
        echo '</div>';
	
    echo '</div>';
    // Pesquisa 
    echo '<div style="box-shadow: 0 0 .4em #a6a6a6; display: flex;margin-bottom: 20px; margin-top: 20px; flex-direction: row; border-radius: 10px;">';
		if( $iPod == true || $iPhone == true || $iPad == true || $iPod_s == true || $iPhone_s == true || $iPad_s == true ){
			echo '<input id="deputado-pesquisa" style="border: none !important; width: 80%; padding: 15px; flex-grow 1;" placeholder="Nome do parlamentar"></input>';
		} else {
			echo '<input id="deputado-pesquisa" style="width: 80%; padding: 15px;  border-radius: 10px; border-top-right-radius: 0; border-bottom-right-radius: 0; flex-grow 1;" placeholder="Nome do parlamentar"></input>';
}
    
    echo '<i onclick="redirect()" class="fa-solid fa-magnifying-glass"></i>';
    echo '<button id="deputado-pesquisa-botao" onclick="redirect()">Pesquisar</button>';
    echo '</div>';
    echo '</div>';
    // Parlamentares e seus respectivos status
    if(str_contains($url, '?') || str_contains($url, '&') and !str_contains($url, '?lista')) {
	    echo '<div style="display: flex; flex-direction: row; flex-wrap: wrap; justify-content: center;">';
		foreach ($usuarios as $usuario) {
			$status = '';
			if ($usuario->acao_1 == 1) {
				$status = 'background-color: #c9efff;';
			}
			echo '<div id="deputado" style="' . $status . '">';
			if ($usuario->email[0] == 's' || $usuario->email[0] == 'S') {
				echo '
				<div style="border-radius: 10px;padding: 6px;color: white;height: 20px;background: #1a447a;position: absolute;right: 21px;">
					<a>Senador</a>
				</div>
				';
			} else {
					echo '
				<div style="border-radius: 10px;padding: 6px;color: white;height: 20px;background: #17b14b;position: absolute;right: 21px;">
					<a>Deputado</a>
				</div>
				';
			}
			if ($usuario->acao_1 == 1) {
				echo '<p style="margin: 0 0 10px 0"; padding: 0;><a href="' . $usuario->url . '" target="_blank"><b>' . $usuario->nome . '</b></a></p>';
			} else {
				echo '<p style="margin: 0 0 10px 0"; padding: 0;><a><b>' . $usuario->nome . '</b></a></p>';
			}
			echo '<p style="margin: 5px; padding: 0; font-size: 90%;">
				  <i class="fa fa-phone" aria-hidden="true" style="color:#1768b1;"></i> <a href="tel:+5561' . $usuario->telefone . '">(61) ' . $usuario->telefone . '</a></p>';
			echo '<p style="margin: 5px; padding: 0; font-size: 90%;">
				  <i class="far fa-envelope" aria-hidden="true" style="color:red;"></i> <a href="mailto:' . $usuario->email . '">' . $usuario->email . '</a></p>';
			echo '<br>';
			echo '<div>';
			echo '<div id="center">';
			echo '<a id="center"><b>Indicou?</b> &nbsp;&nbsp;</a>';
			echo ($usuario->acao_1 == 0) ? '❌' : '✔';
			echo '</div>';
			echo '</div>';
			echo  '</div>';
		}
		echo  '</div>';
		echo '</div>';
		// Paginator
		echo '<div style="font-size: 20px; column-gap: 10px; display: flex; justify-content: center;">';
		echo paginate_links(array(
			'base' => add_query_arg('cpage', '%#%'),
			'format' => '',
			'prev_text' => __('&laquo;'),
			'next_text' => __('&raquo;'),
			'total' => ceil($total / $items_per_page),
			'current' => $page
		));
		echo '<div>';
		echo '</div>';
	}
 
    // CSS
    echo '
    <style>
		#procedimento a:nth-last-child(2):hover { 
			cursor: pointer;
		}
        #center {
            display: flex; 
            justify-content: center;
        }
        #deputados {
            display: flex; 
            flex-direction: column;
        }
        #deputados-container {
            display: flex; 
            flex-direction: column; 
            flex-wrap: wrap;
        }
		#deputados-container > h2 {
			color: #1a447a;
		}
        #instrucoes {
            display: flex; 
            flex-direction: row; 
        }
        #instrucoes-direita {
            width: 40%; 
            padding-top: 0; 
            height: -webkit-fill-available; 
            width: auto;
        }
        #instrucoes-esquerda-titulo {
            border: 1px solid #c9c9c9; 
            border-radius: 10px; 
            padding: 20px; 
			padding-top: 0;
            margin-right: 20px;
        }
		#instrucoes-esquerda-titulo > h4,#instrucoes-esquerda-titulo > h2  {
			color: #1a447a;
		}
        #procedimento {
            margin-bottom: 20px; 
            border: 1px solid #c9c9c9; 
            padding: 20px; border-radius: 10px; 
            padding-top: 0;
        }
		#procedimento > h4 {
			color: #1a447a;
		}
        #baixe {
            border: 1px solid #c9c9c9; 
            padding: 20px;  
            padding-top: 0; 
            border-radius: 10px;
			min-width: 325px;
        }
		#baixe > h4 {
			color: #1a447a;
		}
        #baixe-botoes {
            display: flex;
            flex-direction: row; 
            width: 100%;
        }
        #baixe-botoes > div {
            border-radius: 10px; 
            padding: 10px; 
            margin: 5px; 
            background-color: #e5e5e5; 
            width: 100px; 
            height: 165px;
        }
        #deputado {
			position: relative;
            height: -webkit-fill-available; 
            background-color: #e5e5e5;
            padding: 20px; 
            border-radius: 10px; 
            word-break: break-all; 
            display: flex; 
            flex-direction: 
            column; 
            flex: 0 0 21%; 
            margin: 10px;
            min-width: 325px;
        }
        .fa-magnifying-glass, #deputado-pesquisa-botao {
            width: 20%;  
            border-radius: 10px;  
            border-top-left-radius: 0; 
            border-bottom-left-radius: 0; 
            padding: 15px; border: none; 
            background-color: #1e73be; 
            color: #ffff;
            cursor: pointer
        }
        .fa-magnifying-glass {
            display: none;
        }
        @media (max-width: 780px)
        {
			#procedimento > video {
				width: 100%;
				height: 100%;
			}
            button {
                display: flex;
                align-itens: center;
                justify-content: center;
            }
            #instrucoes {
                flex-wrap: wrap;
            }
            #instrucoes-esquerda, #instrucoes-esquerda-titulo {
                margin-top: 20px;
				margin-right: 0px;
            }
            #instrucoes-direita {
                margin-top: 20px;
                width: 100%;
            }
            #baixe {
				min-width: 0;
                margin-top: 20px;
            }
            #baixe-botoes {
                flex-wrap: wrap;
                justify-content: center;
            }
            .fa-magnifying-glass {
                display: block;
                width: 5%;
            }
            #deputado-pesquisa, deputado-pesquisa-2 {
                width: 95% !important;
            }
            #deputado-pesquisa-botao {
                display: none;
            }
            #deputado {
                flex: auto !important;
                width: 100%;
                min-width: 0px;
            }
        }
        /* Modal */
        .modal {
            display: none; 
            position: fixed; 
            z-index: 1;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%; 
            overflow: auto;
            background-color: rgb(0,0,0);
            background-color: rgba(0,0,0,0.4);
          }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            position: absolute;
            top: 40%;
            left: 50%;
            transform: translate(-50%, -50%);
            padding: 20px;
            border: 1px solid #888;
            width: 500px;
        }
        @media (max-width: 780px)
        {
            .modal-content {
                width: 90%;
            }
        }
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
			margin-top: -20px;
			margin-right: -15px;
        }
        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }
        .modal-content > input {
            box-shadow: 0 0 1em #a6a6a6;
            width: 100%;
            border: 2px solid #c9c9c9;
        }
        .modal-content > button {
            box-shadow: 0 0 1em #a6a6a6;
            width: 100%;
            background-color: #1e73be;
            border: none;
        }
    </style>
    ';
    // Return vazio, concertar futuramente
    return '';
}
