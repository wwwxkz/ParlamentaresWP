<?php
function deputados_listar()
{
    global $wpdb;
	
    $sql = "SELECT * FROM sna_deputados WHERE email like 'dep%'";
	if(isset($_GET['parlamentar'])) {
		$sql .= "AND nome like '%" . $_GET['parlamentar'] . "%'";
	}
	
    $total_query = "SELECT COUNT(1) FROM (${sql}) AS combined_table";
    $total = $wpdb->get_var( $total_query );
    $items_per_page = 24;
    $page = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
    $offset = ( $page * $items_per_page ) - $items_per_page;
    $usuarios = $wpdb->get_results( $sql . " ORDER BY id LIMIT ${offset}, ${items_per_page}" );

    ?>
    <link rel="stylesheet" href="..\wp-content\plugins\sna-deputados\funcionalidades\listar\style.css">
    <script type="text/javascript" src="..\wp-content\plugins\sna-deputados\funcionalidades\listar\script.js"></script>
    <div style="float: left; margin-top: 15px; padding: 0;">
		<div style="display: flex; flex-direction: row; justify-content: space-between;">
			<script>
			function parlamentares_filtrar() {
				let parlamentar = document.getElementById("nome-parlamentar").value;
				window.location.href = "https://campanha.aeronautas.org.br/wp-admin/admin.php?page=deputados_listar&parlamentar=" + parlamentar;
			}
			function voltar(){ 
				window.location.href = "https://campanha.aeronautas.org.br/wp-admin/admin.php?page=deputados_listar";
			}
			</script>
			<h2>Listagem Deputados</h2>
			<div>
				<button onclick="voltar()">Resetar</button>
				<input id="nome-parlamentar" placeholder="Nome do parlamentar"/>
				<button onclick="parlamentares_filtrar()">Pesquisar</button>
			</div>
		</div>
        <form method="post">
            <input type="submit" class="button action" name="pdf" value="Gerar PDF" />
        </form>
        <br>
        <table class="wp-list-table widefat fixed striped table-view-list">
            <thead>
                <tr>
                    <!-- <th>#</th> -->
                    <th>Nome</th>
                    <!-- <th>Partido</th>
                    <th>UF</th>
                    <th>Titular</th>
                    <th>Endereço 1</th>
                    <th>Anexo</th>
                    <th>Endereço 2</th>
                    <th>Gabinete</th>
                    <th>Endereço 3</th> -->
                    <th>Telefone</th>
                    <!-- <th>Fax</th>
                    <th>Mês</th>
                    <th>Dia</th> -->
                    <th>Email</th>
                    <!-- <th>Nome s/acento</th>
                    <th>Tratamento</th>
                    <th>Nome Civil</th> -->
					<th>URL</th>
                    <th>Indicou</th>
                    <th>Editar</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($usuarios as $usuario) {
                ?>
                    <tr>
                        <th><?php echo $usuario->nome; ?></th>
                        <th><?php echo $usuario->telefone; ?></th>
                        <th><?php echo $usuario->email; ?></th>
						<th><?php echo $usuario->url; ?></th>
                        <th><?php if( $usuario->acao_1 == 0 ) {echo 'Não';} else {echo 'Sim';} ?></th>
                        <th>
                            <form method="post">
                                <button type="button" class="button action" onclick="mostrar_popup('<?php echo $usuario->id; ?>-edit');">Editar</button>
                                <input type="submit" class="button action" name="<?php echo $usuario->id; ?>-deletar" value="Deletar" />
                                <div id="<?php echo $usuario->id; ?>-edit" class="modal" style="display: none;">
                                    <input type="hidden" name="id" value="<?php echo $usuario->id; ?>">
                                    <div class="conteudo-modal" style="display: flex; flex-direction: column;">
                                        <span onclick="fechar_popup()" class="fechar">&times;</span>
                                        <label>Nome</label>
                                        <input type="text" name="nome" value="<?php echo $usuario->nome; ?>" placeholder="Nome completo" />
										<label>URL</label>
                                        <input type="text" name="url" value="<?php echo $usuario->url; ?>" placeholder="URL do site oficial" />
                                        <label>Indicou?</label>
                                        <select id="<?php echo $usuario->id; ?>-acao-1" name="deputados-acao-1">
                                            <option value="0">Não</option>
                                            <option <?php if($usuario->acao_1 == "1") echo 'selected="selected"'; ?> value="1">Sim</option>
                                        </select>
                                        <br>
                                        <input type="submit" class="button action" name="editar" value="Editar" />
                                    </div>
                                </div>
                            </form>
                        </th>
                    </tr>
                <?php
                    if (isset($_POST[$usuario->id . '-deletar'])) {
                        $wpdb->get_results("DELETE FROM sna_deputados WHERE id=$usuario->id");
                        echo "<script>window.location.reload()</script>";
                    }
                }
                ?>
            </tbody>
        </table>
        <?php 
            echo paginate_links( array(
                'base' => add_query_arg( 'cpage', '%#%' ),
                'format' => '',
                'prev_text' => __('&laquo;'),
                'next_text' => __('&raquo;'),
                'total' => ceil($total / $items_per_page),
                'current' => $page
            ));
        ?>
    </div>
<?php
}
?>
