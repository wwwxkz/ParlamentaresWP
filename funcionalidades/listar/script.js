var script = document.createElement('script');
script.src = 'https://code.jquery.com/jquery-3.6.0.min.js';
document.getElementsByTagName('head')[0].appendChild(script);

function fechar_popup() {
    modal.style.display = "none";
}

window.onclick = function (event) {
    if (typeof modal !== 'undefined') {
        if (event.target == modal) {  
            fechar_popup();
        }
    }
}

var jquery = function (callback) {
    if (window.jQuery) {
        callback(jQuery);
    }
    else {
        window.setTimeout(function () { jquery(callback); }, 20);
    }
};

function mostrar_popup(id) {
    modal = document.getElementById(id);
    if (modal.style.display == "none") {
        modal.style.display = "block";
    } else {
        modal.style.display = "none";
    }
}
