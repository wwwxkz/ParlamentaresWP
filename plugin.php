<?php

/*
Plugin Name: WP SNA - Parlamentares
Description: Plugin para monitorar atos de parlamentares desenvolvido por Marcelo Campos
Version: 0.1
Author: Marcelo Rodrigues
Author URI: https://github.com/wwwxkz
*/

define('DEPUTADOS_LOCAL', plugin_dir_path(__FILE__));
require_once(DEPUTADOS_LOCAL . 'funcionalidades/listar/listar.php');
require_once(DEPUTADOS_LOCAL . 'funcionalidades/editar/editar.php');

add_action( 'plugins_loaded', 'deputados_init' );

function deputados_init()
{
    // if(is_super_admin())
	add_action('admin_menu', 'deputados');
}

function deputados(){
	add_menu_page( 'Parlamentares', 'Parlamentares', 'manage_options', 'deputados', 'deputados', 'dashicons-groups', 10 );
	add_submenu_page( 'deputados', 'Listar', 'Listar', 'read', 'deputados_listar', 'deputados_listar' );
	remove_submenu_page('deputados','deputados');
}

register_activation_hook(__FILE__, 'deputados_ativacao');
function deputados_ativacao(){
	global $wpdb;
	$tabela = "sna_deputados";
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $tabela (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		-- Deputados CSV imported info
		nome tinytext NOT NULL,
		partido tinytext NOT NULL,
		uf tinytext NOT NULL,
		titular tinytext NOT NULL,
		endereco_1 tinytext NOT NULL,
		anexo tinytext NOT NULL,
		endereco_2 tinytext NOT NULL,
		gabinete tinytext NOT NULL,
		endereco_3 tinytext NOT NULL,
		telefone tinytext NOT NULL,
		fax tinytext NOT NULL,
		mes tinytext NOT NULL,
		dia tinytext NOT NULL,
		email tinytext NOT NULL,
		nome_sem_acento tinytext NOT NULL,
		tratamento tinytext NOT NULL,
		nome_civil tinytext NOT NULL,
		-- Monitor and register their actions
		acao_1 tinytext NOT NULL,
		-- URL to gov website of the parlamentar
		url tinytext NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);
}

register_uninstall_hook(__FILE__, 'deputados_desinstalacao');
function deputados_desinstalacao(){
	$tabela = "sna_deputados";
	$sql = "DROP TABLE IF EXISTS $tabela";
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);
}
?>
